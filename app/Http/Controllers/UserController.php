<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Validator;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function index(Request $req)
    {
        $results = User::orderBy('id','ASC');
        if($req->has('name')) $results = $results->where('name','LIKE','%'.$req->name.'%');
        if($req->has('email')) $results = $results->where('email','LIKE','%'.$req->email.'%');
        if($req->has('status')) $results = $results->where('status','LIKE','%'.$req->status.'%');
        return response()->json($results->paginate(10));
    }

    public function search(String $key)
    {
        return User::orderByDesc('id', 'DESC')
        ->where('name','LIKE', '%'.$key.'%')
        ->orWhere('email','LIKE', '%'.$key.'%')
        ->paginate(10);
    }

    public function login(Request $request)
    {
        $status = 401;
        $response = ['error' => 'Unauthorised'];

        if (Auth::attempt($request->only(['email','password']))) {
            $user = Auth::user();
            $status = 200;
            $response = [
                'user' => $user,
                'permissions' => $user->getPermissionsViaRoles(),
                'token' => $user->createToken(config('app.name'))->accessToken,
            ];
        }

        return response()->json($response, $status);
    }

    public function create(Request $request)
    {
        if(!isSuper()) {
            return response()->json(['error' => 'Dont have permission'], 401);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50',
            'email' => 'required|email',
            'password' => 'required|min:6',
            'role_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = $request->only(['name','email','password','role_id','status','profile_image']);
        $data['password'] = bcrypt($data['password']);
        
        $user = User::create($data);

        $role_r = Role::where('id', '=', $data['role_id'])->firstOrFail();            
        $user->assignRole($role_r); //Assigning role to user

        return response()->json([
            'user' => $user,
            'token' => $user->createToken(config('app.name'))->accessToken,
        ]);
    }

    public function update(Request $request, User $user)
    {
        $data = $request->only(['name', 'email', 'role_id','status','profile_image']);

        $status = $user->update($data);

        $role_r = Role::where('id', '=', $data['role_id'])->firstOrFail();            
        $user->syncRoles([$role_r]); //Assigning role to user

        return response()->json([
            'status' => $status,
            'message' => $status ? 'User Details Updated!' : 'Error Updating User Details'
        ]);
    }

    public function show(User $user)
    {
        return response()->json('$user');
    }

    public function destroy(Request $request, User $user)
    {
        $status = User::where('id', $request->id)->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'User Deleted!' : 'Error Deleting User'
        ]);
    }

    public function uploadImage(Request $request)
    {
        if($request->hasFile('file')) {

            $uid = auth()->user()->id;
            $files = glob('images/profile/*'); // get all file names
            foreach($files as $file) { // iterate files
                if (strpos($file, "user".$uid) !== false) { //if same naming exists
                    unlink($file); // delete file
                }
            }
            $name = "user".$uid.".".$request->file('file')->getClientOriginalExtension();
            $status = $request->file('file')->move(public_path('images/profile'), $name);
        }
        return response()->json([
            'success' => $status,
            'image' => asset("images/profile/$name")
        ],201);
    }
}
