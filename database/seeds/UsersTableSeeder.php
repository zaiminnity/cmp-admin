<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Admin',
                'email' => 'cmp.admin@innity.com',
                'password' => bcrypt('5up3r4dm1n'),
                'role_id' => 1,
                'status' => 1,
            ]
        ];

        DB::table('users')->insert($users);
    }
}
