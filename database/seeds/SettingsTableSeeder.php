<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            [
                'key' => 'brand_logo',
                'value' => '../images/logo.png',
            ],
            [
                'key' => 'brand_logo_inverse',
                'value' => '../images/logo-inverse.png',
            ],
            [
                'key' => 'brand_name',
                'value' => 'CMP',
            ],
            [
                'key' => 'ahc_brand_id',
                'value' => '29440f48-f7c8-4ac1-8ed1-9b1b5d37124e',
            ],
            [
                'key' => 'ahc_access_token',
                'value' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjNjYjk4ZWM1Y2ZiY2E1ODcxZmJmZGExMzYyMTQxM2QwZjVhYmFjZTgwYWZkYTZlMjRiMjdlMDY2MGIxZTQ3MmRhNThiZDhmNDI2ZDI0ZDM1In0.eyJhdWQiOiIxIiwianRpIjoiM2NiOThlYzVjZmJjYTU4NzFmYmZkYTEzNjIxNDEzZDBmNWFiYWNlODBhZmRhNmUyNGIyN2UwNjYwYjFlNDcyZGE1OGJkOGY0MjZkMjRkMzUiLCJpYXQiOjE1ODMyMjU1ODMsIm5iZiI6MTU4MzIyNTU4MywiZXhwIjoxODk4NzU4MzgzLCJzdWIiOiI1MSIsInNjb3BlcyI6W119.NYBYD45ujgAaVDekVRGMaGW7Zs-PM96IexQlc_aE2e-2Zjd61SKj9Y8_3Nfbf5z-ql8wu3TRd5MMsRnjIjFCoZmfXwt2F0k1DNOB6dEE0tuNPU2sD3xdOwJg4LBFTcTSuUF8efx_0dYVuTLKSuMM4rkeIAeChgpmuESTipibKCziT3Jzny0y0VGVLE3kTo8iQ6F5xwpt2w0x-MF-KXSrRP8o3Gh_K8-nDzwVuHcWUY_CrkvHkI_8SrZY_cIem39EPpQEo0Q_wy9pjspD5WJdUsgTnbxf53U16uZp4KwfpsqQJowaS1UTEsINaN3baNCng86WUnjS-rMXkhJTTHjdo71CazZLmnb3KjFNgvx0JrhRjp-LXoB2AWM9lZhwJm6BQBAXM0HSKX7FwTyu2IEzMNIBli6IysUNqFZxGCQV7MtFLoh78k84tbeOPtoeARsVrYFdXx7kvHaCRq3YEO9bRQmcawTQpvgh8tidSYPPoLQiJwWmJ7sUZBzTGLHEhvYlthNGDJ8EEjG8gCp9xAIOy6Ag7WcUn55S8jFZWDVwawnzzbID_54m0geMmmP1px7FI1RoylmlMBF2NsqK4FjMN_d3_XTg-hFRqQPtkz5NZrtxm4NttIZHXSlx2CLNYlkRz9YkOUR7piXfY2_5KoOwmIl9kAsCIlDFpo6YiXeJAKI',
            ],
            [
                'key' => 'live_url',
                'value' => 'https://live_url.example.com',
            ],
            [
                'key' => 'staging_url',
                'value' => 'https://staging.example.com',
            ],
        ];

        DB::table('settings')->insert($settings);
    }
}
