<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' => 'everything',
                'guard_name' => 'api',
            ],
            [
                'name' => 'user_add',
                'guard_name' => 'api',
            ],
            [
                'name' => 'user_edit',
                'guard_name' => 'api',
            ],
            [
                'name' => 'user_delete',
                'guard_name' => 'api',
            ],
            [
                'name' => 'content_add',
                'guard_name' => 'api',
            ],
            [
                'name' => 'content_edit',
                'guard_name' => 'api',
            ],
            [
                'name' => 'content_delete',
                'guard_name' => 'api',
            ]
        ];

        DB::table('permissions')->insert($permissions);
    }
}
