import Vue from 'vue'
import './plugins/vuetify';
import BootstrapVue from "bootstrap-vue"
import VueRouter from 'vue-router'
// import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/dist/vuetify.min.css'
import Vuetify from 'vuetify'
import Vuex from 'vuex'
import VueElementLoading from 'vue-element-loading'


Vue.use(BootstrapVue);
Vue.use(VueRouter);
Vue.use(Vuetify);
Vue.use(Vuex);


import App from './views/App'

// import Categories from './views/admin/Categories'
// import Category from './views/admin/Category'
import Content from './views/admin/Content'
import Contents from './views/admin/Contents'
import ContentTypes from './views/admin/ContentTypes'
import ContentType from './views/admin/ContentType'
import Dashboard from './views/admin/Dashboard'
import Login from './views/admin/Login'
import Users from './views/admin/Users'
import Profile from './views/admin/Profile'
import Permissions from './views/admin/Permissions'
import Roles from './views/admin/Roles'
import Settings from './views/admin/Settings'
// import Survey from './views/admin/Survey'

import Default from './layout/wrappers/baseLayout.vue';
import Pages from './layout/wrappers/pagesLayout.vue';

window.store = new Vuex.Store({
    state:{
      query:{},
      url:''
    },
    mutations:{
      queryData(state,data){
        state.query=data;
      },
      urlData(state,data){
        state.url=data;
      }
    }

});

Vue.config.productionTip = false;

Vue.component('admin-layout', Default);
Vue.component('pages-layout', Pages);
Vue.component('VueElementLoading', VueElementLoading);


let user = {};

const router = new VueRouter({
  // base: '/2020/my/admin_demo',
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard,
      meta: {
        requiresAuth: true,
        layout: 'admin'
      }
    },
    // {
    //   path: '/categories',
    //   name: 'categories',
    //   component: Categories,
    //   meta: {
    //     requiresAuth: true,
    //     superAdmin: true,
    //     permission: 'user_add',
    //     layout: 'admin',
    //   }
    // },
    // {
    //   path: '/category/:id',
    //   name: 'category',
    //   component: Category,
    //   meta: {
    //     requiresAuth: true,
    //     permission: 'user_add',
    //     layout: 'admin',
    //   }
    // },
    {
      path: '/content/:id',
      name: 'content',
      component: Content,
      meta: {
        requiresAuth: true,
        permission: 'everything',
        layout: 'admin',
      }
    },
    {
      path: '/content/new/:type',
      name: 'content-new',
      component: Content,
      meta: {
        requiresAuth: true,
        permission: 'everything',
        layout: 'admin',
      }
    },
    {
      path: '/contents',
      name: 'contents',
      component: Contents,
      meta: {
        requiresAuth: true,
        superAdmin: false,
        permission: 'content_add',
        layout: 'admin',
      }
    },
    {
      path: '/content-types',
      name: 'content-types',
      component: ContentTypes,
      meta: {
        requiresAuth: true,
        superAdmin: false,
        permission: 'content_add',
        layout: 'admin',
      }
    },
    {
      path: '/content-type/:id',
      name: 'content-type',
      component: ContentType,
      meta: {
        requiresAuth: true,
        permission: 'everything',
        layout: 'admin',
      }
    },
    {
      path: '/content-type/new/:type',
      name: 'content-type-new',
      component: ContentType,
      meta: {
        requiresAuth: true,
        permission: 'everything',
        layout: 'admin',
      }
    },
    // {
    //   path: '/dashboard',
    //   name: 'dashboard',
    //   component: Dashboard,
    //   meta: {
    //     requiresAuth: true,
    //     layout: 'admin'
    //   }
    // },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/permissions',
      name: 'permissions',
      component: Permissions,
      meta: {
        requiresAuth: true,
        superAdmin: true,
        permission: 'everything',
        layout: 'admin',
      }
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
      meta: {
        layout: 'admin',
      }
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings,
      meta: {
        requiresAuth: true,
        superAdmin: true,
        permission: 'everything',
        layout: 'admin',
      }
    },
    {
      path: '/roles',
      name: 'roles',
      component: Roles,
      meta: {
        requiresAuth: true,
        superAdmin: true,
        permission: 'everything',
        layout: 'admin',
      }
    },
    {
      path: '/users',
      name: 'users',
      component: Users,
      meta: {
        requiresAuth: true,
        superAdmin: true,
        permission: 'everything',
        layout: 'admin',
      }
    },
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    //not logged in
    if (localStorage.getItem(process.env.MIX_APP_NAME + '.jwt') == null) {
      if (to.path.indexOf('admin') >= 0 && to.path.indexOf('login') < 0) {
        next({
          path: '/admin/login',
          params: { nextUrl: to.fullPath }
        });
      }
      else {
        next({
          path: '/login',
          params: { nextUrl: to.fullPath }
        });
      }
    }

    //logged in
    else {
      if (to.matched.some(record => record.meta.superAdmin)) {
        if (true) {
          next()
        }
        else {
          next({ name: 'dashboard' })
        }
      }
      next()
    }
  } else {
    next()
  }
});

const app = new Vue({
  el: '#app',
  template: '<App/>',
  components: { App },
  vuetify: new Vuetify(),
  router,
});

function userCan(name) {
  user = JSON.parse(localStorage.getItem(process.env.MIX_APP_NAME + '.user'));
  return user.roles[0].permissions.find(e => e.name == name) != undefined;
}