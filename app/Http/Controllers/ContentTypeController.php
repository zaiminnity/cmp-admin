<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Libraries\AHC_SDK;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContentTypeController extends Controller
{
    private $ahc;
    private $brandId = '';

    function __construct()
    {
        $this->ahc = new AHC_SDK();
        $this->brandId = DB::table('settings')->where('key','ahc_brand_id')->value('value');
    }

    public function index(Request $request)
    {
        $params = ['brand_id'=>$this->brandId];
        foreach($request->all() as $key => $value) {
            $params[$key] = $value;
        }
        
        $result = $this->ahc->getContentTypeList($params);
        return response()->json($result);
    }

    public function show(Request $request,$contentType)
    {
        $params = ['default_fields'=>'true'];
        $result = $this->ahc->getContentType($contentType,$params);
        return response()->json($result);
    }
    
    public function indexBrand(Request $request)
    {
        $params = [$this->brandId];
        foreach($request->all() as $key => $value) {
            $params[$key] = $value;
        }
        
        $result = $this->ahc->getBrandList($params);
        return response()->json($result);
    }

    public function update(Request $request,$contentType)
    {
        $params = ['brand_id'=>$this->brandId];
        foreach($request->all() as $key => $value) {
            $params[$key] = $value;
        }
        $result = $this->ahc->updateContentType($params,$contentType);
        return response()->json($result);
    }

    public function delete(Request $request,$contentType)
    {
        $result = $this->ahc->deleteContentType($contentType);
        return response()->json($contentType);
    }

    public function create(Request $request)
    {
        $params = ['brand_id'=>$this->brandId];
        foreach($request->all() as $key => $value) {
            $params[$key] = $value;
        }
        
       $result = $this->ahc->insertContentType($params);
        return response()->json($result);
    }

    public function userCan(String $id)
    {
        $user = Auth::user();
        if($user->hasPermissionTo($id,'api') || $user->hasPermissionTo('everything','api') ) {
            return true;
        }
        else return false;
    }
}
