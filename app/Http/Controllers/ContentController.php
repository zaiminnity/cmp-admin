<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Libraries\AHC_SDK;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContentController extends Controller
{
    private $ahc;
    private $brandId = '';

    function __construct()
    {
        $this->ahc = new AHC_SDK();
        $this->brandId = DB::table('settings')->where('key','ahc_brand_id')->value('value');
    }

    public function index(Request $request)
    {
        $params = ['brand_id'=>$this->brandId];
        foreach($request->all() as $key => $value) {
            $params[$key] = $value;
        }

        $result = $this->ahc->getContentList($params);
        return response()->json($result);
    }

    public function show($id)
    {
        $result = $this->ahc->getContentList([],$id);
        return response()->json($result);
    }

    public function create(Request $request)
    {
        if(!$this->userCan('content_add')) {
            return response()->json(['success'=>false,'error'=>'Unauthorized']);
        }

        $params = ['brand_id'=>$this->brandId];
        foreach($request->all() as $key => $value) {
            $params[$key] = $value;
        }

        $result = $this->ahc->insertContent($params);
        return response()->json($result);
    }

    public function createSocial(Request $request)
    {
        if(!$this->userCan('content_add')) {
            return response()->json(['success'=>false,'error'=>'Unauthorized']);
        }

        $params = ['brand_id'=>$this->brandId];
        foreach($request->all() as $key => $value) {
            $params[$key] = $value;
        }

        $result = $this->ahc->insertSocialContent($params);
        return response()->json($result);
    }

    public function update(Request $request, $id)
    {
        if(!$this->userCan('content_edit')) {
            return response()->json(['success'=>false,'error'=>'Unauthorized']);
        }

        $params = ['brand_id'=>$this->brandId];
        foreach($request->all() as $key => $value) {
            $params[$key] = $value;
        }

        $result = $this->ahc->updateContent($params,$id);
        return response()->json($result);
    }

    public function uploadImage(Request $request)
    {
        if($request->hasFile('file')) {
            $name = $request->name.".".$request->file('file')->getClientOriginalExtension();
            $status = $request->file('file')->move(public_path('images/temp'), $name);
        }
        
        return response()->json([
            'status' => $status,
            'image' => asset("images/temp/$name")
        ],201);
    }

    public function delete(Request $request, $id)
    {
        if(!$this->userCan('content_delete')) {
            return response()->json(['success'=>false,'error'=>'Unauthorized']);
        }

        $result = $this->ahc->deleteContent($id);
        return response()->json($result);
    }

    // public function type(Request $request)
    // {
    //     $params = ['brand_id'=>$this->brandId];
    //     foreach($request->all() as $key => $value) {
    //         $params[$key] = $value;
    //     }

    //     $result = $this->ahc->getContentTypeList($params);
    //     return response()->json($result);
    // }

    // public function type_info(Request $request, $content)
    // {
        
    //     $result = $this->ahc->getContentType($content);
    //     return response()->json($result);
    // }
    
    // public function update_type(Request $request,$contentType)
    // {
    //     $result = $this->ahc->updateContentType($request,$contentType);
    //     return response()->json($result);
    // }

    // public function delete_type(Request $request,$contentType)
    // {
    //     $result = $this->ahc->deleteContentType($contentType);
    //     return response()->json($contentType);
    // }
  
    public function country()
    {
        $result = $this->ahc->getCountryList();
        return response()->json($result);
    }

    public function stats()
    {
        $result = $this->ahc->getStats();
        return response()->json($result);
    }

    public function userCan(String $id)
    {
        $user = Auth::user();
        if($user->hasPermissionTo($id,'api') || $user->hasPermissionTo('everything','api') ) {
            return true;
        }
        else return false;
    }
}
