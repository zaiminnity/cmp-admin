<?php

function isSuper()
{
    $user = Auth::user();
    return $user->hasRole('Super Admin');
}