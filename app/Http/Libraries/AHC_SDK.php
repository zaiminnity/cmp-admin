<?php

namespace App\Http\Libraries;

use Illuminate\Support\Facades\DB;
use Curl\Curl;
use Artisan;
use Cache;

class AHC_SDK
{
    private $apiUrl = '';
    private $endpoint = '';
    private $paramStr = '';
    private $params = [];
    private $request_url = '';
    private $curl;

    function __construct()
    {
        if (!in_array('curl', get_loaded_extensions())) {
            throw new \Exception('You need to install cURL, see: https://curl.haxx.se/docs/install.html');
        }
        $this->apiUrl = env('AHC_URL');
        $this->curl = new Curl();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // brand
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function insertBrand($params = [])
    {
        $this->endpoint = 'brand';
        $this->params = $params;
        $result = $this->postApi();
        return $result;
    }

    function updateBrand($params = [], $id = '')
    {
        $this->endpoint = 'brand/' . $id;
        $this->params = $params;
        $result = $this->putApi();
        return $result;
    }

    function deleteBrand($id = '')
    {
        $this->endpoint = 'brand/' . $id;
        $result = $this->deleteApi();
        return $result;
    }

    function addBrandSource($params = [])
    {
        $this->endpoint = 'source';
        $this->params = $params;
        $result = $this->postApi();
        return $result;
    }

    function getBrandList($params = [], $id = '')
    {
        $this->endpoint = empty($id) ? 'brand' : 'brand/' . $id;
        $this->id = $id;
        $this->paramStr = '&' . http_build_query($params);
        $result = $this->getApi();
        return $result;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // source
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function createBrandSource($params = [])
    {
        $this->endpoint = 'source';
        $this->params = $params;
        $result = $this->postApi();
        return $result;
    }

    function updateBrandSource($params = [], $id = '')
    {
        $this->endpoint = 'source/' . $id;
        $this->params = $params;
        $result = $this->putApi();
        return $result;
    }

    function deleteBrandSource($id = '')
    {
        $this->endpoint = 'source/' . $id;
        $result = $this->deleteApi();
        return $result;
    }

    function getBrandSourceList($params = [], $id = '')
    {
        $this->endpoint = empty($id) ? 'source' : 'source/' . $id;
        $this->id = $id;
        $this->paramStr = '&' . http_build_query($params);
        $result = $this->getApi();
        return $result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //categories
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function insertCategory($params = [])
    {
        $this->endpoint = 'category';
        $this->params = $params;
        $result = $this->postApi();
        return $result;
    }

    function updateCategory($params = [], $id = '')
    {
        $this->endpoint = 'category/' . $id;
        $this->params = $params;
        $result = $this->putApi();
        return $result;
    }

    function deleteCategory($id = '')
    {
        $this->endpoint = 'category/' . $id;
        $result = $this->deleteApi();
        return $result;
    }

    function getCategoryListByBrandID($params = [])
    {
        $this->endpoint = 'category';
        $this->paramStr = '&' . http_build_query($params);
        $result = $this->getApi();
        return $result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //content
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function insertContent($params = [])
    {
        $this->endpoint = 'content';
        $this->params = $params;
        $result = $this->postApi();
        return $result;
    }

    function updateContent($params = [], $id = '')
    {
        $this->endpoint = 'content/' . $id;
        $this->params = $params;
        $result = $this->putApi();
        return $result;
    }

    function updateContentLabel($params = [])
    {
        $this->endpoint = 'content-label';
        $this->params = $params;
        $result = $this->postApi();
        return $result;
    }

    function deleteContent($id = '')
    {
        $this->endpoint = 'content/' . $id;
        $result = $this->deleteApi();
        return $result;
    }

    function getContentList($params = [], $id = '')
    {
        $this->endpoint = empty($id) ? 'content' : 'content/' . $id;
        $this->paramStr = '&' . http_build_query($params);
        $result = $this->getApi();
        return $result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //content_type
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function insertContentType($params = [])
    {
        $this->endpoint = 'content-type';
        $this->params = $params;
        $result = $this->postApi();
        return $result;
    }

    function updateContentType($params = [], $name = '')
    {
        $this->endpoint = 'content-type' . '/' . $name;
        $this->params = $params;
        $result = $this->putApi();
        return $result;
    }

    function getContentTypeList($params = [])
    {
        $this->endpoint = 'content-type';
        $this->paramStr = '&' . http_build_query($params);
        $result = $this->getApi();
        return $result;
    }

    function getContentType($id,$params = [])
    {
        $this->endpoint = 'content-type/'.$id;
        $this->paramStr = http_build_query($params);
        $result = $this->getApi();
        return $result;
    }

    function getCountryList($params = [])
    {
        $this->endpoint = 'country';
        $this->paramStr = '&' . http_build_query($params);
        $result = $this->getApi();
        return $result;
    }

    function deleteContentType($params = '')
    {
        $this->endpoint = 'content-type/' . $params;
        $result = $this->deleteApi();
        return $result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //label
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function getLabelList($params = [])
    {
        $this->endpoint = 'label';
        $this->paramStr = '&' . http_build_query($params);
        $result = $this->getApi();
        return $result;
    }

    function addArchiveLabel($params = [], $label = '')
    {
        $this->endpoint = 'label/' . $label;
        $this->params = $params;
        $result = $this->putApi();
        return $result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //hashtag
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function getHashtags($params = []){
        $this->endpoint = 'hashtag';
        $this->paramStr = http_build_query($params);
        $result = $this->getApi();
        return $result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //social content
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function insertSocialContent($params = []){
        $this->endpoint = 'social-content';
        $this->params = $params;
        $result = $this->postApi();
        return $result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //user
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function userAuth($params = [])
    {
        $this->endpoint = 'oauth/token';
        $this->params = $params;
        $this->request_url = env('AHC_URL_AUTH') . $this->endpoint;
        $this->setAuth();
        $result = $this->curl->post($this->request_url, json_encode($this->params));
        return $result;
    }

    function getUserList($params = [], $id = '')
    {
        $this->endpoint = empty($id) ? 'user' : 'user/' . $id;
        $this->paramStr = '&' . http_build_query($params);
        $result = $this->getApi();
        return $result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //social account
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function postSocialAccount($params = [])
    {
        $this->endpoint = 'social-account';
        $this->params = $params;
        $result = $this->postApi();
        return $result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //stats
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function getStats()
    {
        $this->endpoint = 'stats';
        $result = $this->getApi();
        return $result;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //api call
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function getApi()
    {
        if (!Cache::has($this->endpoint . '?' . $this->paramStr)) {
            $this->request_url = env('AHC_URL') . $this->endpoint . '?' . $this->paramStr;
            $this->setAuth();
            $result = $this->curl->get($this->request_url);
            // Cache::forever($this->endpoint . '?' . $this->paramStr, $result);
        }
        else {
            $result = Cache::get($this->endpoint . '?' . $this->paramStr);
        }

        return $result;
    }

    function postApi()
    {
        Artisan::call('cache:clear');
        $this->request_url = env('AHC_URL') . $this->endpoint;
        $this->setAuth();
        $result = $this->curl->post($this->request_url, json_encode($this->params));
        return $result;
    }

    function putApi()
    {
        Artisan::call('cache:clear');
        $this->request_url = env('AHC_URL') . $this->endpoint;
        $this->setAuth();
        $result = $this->curl->put($this->request_url, json_encode($this->params));
        return $result;
    }

    function deleteApi()
    {
        Artisan::call('cache:clear');
        $this->request_url = env('AHC_URL') . $this->endpoint;
        $this->setAuth();
        $result = $this->curl->delete($this->request_url);
        return $result;
    }

    function setAuth()
    {
        $this->curl->setOpt(CURLOPT_HEADER, false);
        $this->curl->setOpt(CURLOPT_RETURNTRANSFER, true);
        $this->curl->setHeader('Content-Type', 'application/json');
        $this->curl->setHeader('Authorization', 'Bearer ' . DB::table('settings')->where('key','ahc_access_token')->value('value'));
    }
}