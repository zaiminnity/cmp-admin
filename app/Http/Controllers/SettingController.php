<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{

    public function index(Request $request)
    {
        $results = DB::table('settings')->get();
        return response()->json($results);
    }

    public function update(Request $request)
    {
        $status = true;
        foreach($request->all() as $key => $value) {
            $_status = DB::table('settings')->where('key', $key)->update(['value'=>$value]);
            if(!$status) $status = $_status;
        }

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Settings Updated!' : 'Error Updating Settings'
        ]);
    }

    public function uploadImage(Request $request)
    {
        if($request->hasFile('file')) {
            $name = $request->name.".".$request->file('file')->getClientOriginalExtension();
            $request->file('file')->move(public_path('images'), $name);

            $key = ($request->name == 'logo') ? 'brand_logo' : 'brand_logo_inverse';
            $status = DB::table('settings')->where('key', $key)->update(['value'=>'../images/'.$name]);
        }
        
        return response()->json([
            'status' => $status,
            'image' => asset("images/$name")
        ],201);
    }
}
