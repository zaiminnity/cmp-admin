<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Session;

//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);//isAdmin middleware lets only users with a //specific permission permission to access these resources
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $permissions = Permission::orderBy('name')->paginate(10);

        return response()->json($permissions);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request)
    {
        if(!isSuper()) {
            return response()->json(['error' => 'Dont have permission'], 401);
        }

        $this->validate($request, [
            'name'=>'required|max:40',
        ]);

        $name = $request['name'];
        $permission = new Permission();
        $permission->name = $name;

        $roles = $request['roles'];

        $permission->save();

        if (!empty($request['roles'])) { //If one or more role is selected
            foreach ($roles as $role) {
                $r = Role::where('id', '=', $role)->firstOrFail(); //Match input role to db record

                $permission = Permission::where('name', '=', $name)->first(); //Match input //permission to db record
                $r->givePermissionTo($permission);
            }
        }

        return response()->json([
            'permission' => $permission,
            'message' => 'Permission Added!'
        ]);
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        return redirect('permissions');
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        if(!isSuper()) {
            return response()->json(['error' => 'Dont have permission'], 401);
        }

        $permission = Permission::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:40',
        ]);
        $input = $request->all();
        $permission->fill($input)->save();

        return response()->json([
            'message' => 'Permission Updated!'
        ]);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request)
    {
        if(!isSuper()) {
            return response()->json(['error' => 'Dont have permission'], 401);
        }
        
        $permission = Permission::findOrFail($request->id);

        //Make it impossible to delete this specific permission    
        if ($permission->name == "everything") {
            return response()->json([
                'status' => false,
                'message' => 'Cannot Delete This Permission!'
            ]);
        }

        $permission->delete();

        return response()->json([
            'status' => true,
            'message' => 'Permission Deleted!'
        ]);
    }
}
