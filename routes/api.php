<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'UserController@login');

Route::group(['middleware' => 'auth:api'], function(){

    Route::post('users/create', 'UserController@create');
    Route::get('/users', 'UserController@index');
    Route::get('users/{user}', 'UserController@show');
    Route::get('/users/search/{keyword}', 'UserController@search');
    Route::patch('users/{user}', 'UserController@update');
    Route::delete('/users', 'UserController@destroy');
    Route::post('users/image', 'UserController@uploadImage');

    Route::post('roles/create', 'RoleController@create');
    Route::get('/roles', 'RoleController@index');
    Route::get('roles/{role}', 'RoleController@show');
    Route::patch('roles/{role}', 'RoleController@update');
    Route::delete('/roles', 'RoleController@destroy');

    Route::post('permissions/create', 'PermissionController@create');
    Route::get('/permissions', 'PermissionController@index');
    Route::get('permissions/{perm}', 'PermissionController@show');
    Route::patch('permissions/{perm}', 'PermissionController@update');
    Route::delete('/permissions', 'PermissionController@destroy');
    
    Route::get('/contents', 'ContentController@index');
    Route::get('/contents/{content}', 'ContentController@show');

    Route::put('/content', 'ContentController@create');
    Route::put('/content/{content}', 'ContentController@update');
    Route::post('/content/image', 'ContentController@uploadImage');
    Route::delete('/content/{content}', 'ContentController@delete');
    
    Route::get('/content-types', 'ContentTypeController@index');
    Route::get('/content-type/{content}', 'ContentTypeController@show');
    Route::get('/content-types/brand', 'ContentTypeController@indexBrand');
    Route::delete('/content-type/{content}', 'ContentTypeController@delete');
    Route::post('/content-type/{content}', 'ContentTypeController@create');
    Route::put('/content-type/{content}', 'ContentTypeController@update');

    Route::post('/social-content', 'ContentController@createSocial');
    Route::get('/country', 'ContentController@country');
    Route::get('/stats', 'ContentController@stats');
    
    Route::get('/settings', 'SettingController@index');
    Route::patch('/settings', 'SettingController@update');
    Route::post('/settings/image', 'SettingController@uploadImage');
});

// original
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
